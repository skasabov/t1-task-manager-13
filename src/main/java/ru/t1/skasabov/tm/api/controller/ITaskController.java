package ru.t1.skasabov.tm.api.controller;

public interface ITaskController {

    void clearTasks();

    void createTask();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void showTasks();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskById();

    void changeTaskByIndex();

}
