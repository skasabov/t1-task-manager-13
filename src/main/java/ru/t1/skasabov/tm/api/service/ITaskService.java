package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void add(Task task);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task create(String name, String description);

    Task create(String name);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    List<Task> findAll();

    void clear();

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
