package ru.t1.skasabov.tm.repository;

import ru.t1.skasabov.tm.api.repository.IProjectRepository;
import ru.t1.skasabov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project: projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projects.remove(project);
    }

    @Override
    public void removeById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return;
        remove(project);
    }

    @Override
    public int getSize() {
        return projects.size();
    }

}
